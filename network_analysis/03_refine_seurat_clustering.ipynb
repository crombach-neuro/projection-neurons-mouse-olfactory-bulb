{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 03_refine_seurat_clustering\n",
    "\n",
    "Jupyter notebook for the study \"Molecular characterization of projection neuron subtypes in the mouse olfactory bulb\". See also our manuscript: <https://www.biorxiv.org/content/10.1101/2020.11.30.405571>\n",
    "\n",
    "June 2021\n",
    "\n",
    "Contact:\n",
    "\n",
    "- Sara Zeppilli, sara_zeppilli@brown.edu\n",
    "- Robin Attey, robin_attey@alumni.brown.edu\n",
    "- Anton Crombach, anton.crombach@inria.fr\n",
    "\n",
    "or any of the other authors on the manuscript."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import pickle\n",
    "\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import scipy.spatial.distance as sci_distance\n",
    "import scipy.cluster.hierarchy as sci_hierarchy\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "import seaborn as sns\n",
    "\n",
    "import anndata\n",
    "import scanpy as sc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Input paths etc."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Input paths and files are organized in two folders:\n",
    "# - the (raw) expression matrix from 10X, subcluster only; with a file for \n",
    "#   2000 highly variable genes and a file of nuc ids excluding the PG cluster,\n",
    "# - lists of regulons at different confidence cut-offs. These are pickled python objects\n",
    "\n",
    "# Path to expression matrix and list of nuclei excluding the PG cluster\n",
    "EXPR_PATH = \"../../data/2021-04-08_revised_subcluster\"\n",
    "\n",
    "# Looking only at excitatory neurons\n",
    "EXPR_MATRIX_FNAME = 'elife_revisions_subcluster.loom'\n",
    "# Nuclei identifiers filtered for PG cells\n",
    "IDS_NOPG_FNAME = \"elife_revisions_nuclei_ids_noPG.txt\"\n",
    "\n",
    "# Output path for regulons and their AUC matrix (\"regulon activity matrix\")\n",
    "REGULONS_PATH = \"../../data/2021-05-19_revised_regulons_postprocessed\"\n",
    "\n",
    "# Do not need regulons here, just their activity\n",
    "cutoff = 'r0.8_t0.8_mt8'\n",
    "AUC_MATRIX_FNAME = \"subcluster_auc_mtx_{}.p\".format(cutoff)\n",
    "\n",
    "# Output of figures\n",
    "FIG_PATH = \"./figures\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Read in AUC matrix"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Read the activity matrix computed from the pruned/united regulons\n",
    "with open(os.path.join(REGULONS_PATH, AUC_MATRIX_FNAME), 'rb') as infile:\n",
    "    auc_mtx = pickle.load(infile)\n",
    "\n",
    "# Hard zeros are missing data\n",
    "auc_mtx[auc_mtx < 1e-5] = np.nan\n",
    "\n",
    "# Make a standardized version so we can compare cells\n",
    "auc_mtx_Z = pd.DataFrame(index=auc_mtx.index)\n",
    "for col in list(auc_mtx.columns):\n",
    "    avg = auc_mtx[col].mean()\n",
    "    auc_mtx_nona = auc_mtx[col].fillna(avg)\n",
    "    auc_mtx_Z[col] = (auc_mtx_nona - avg) / auc_mtx_nona.std(ddof=0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Read in subcluster expression data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Seurat-style UMAP coordinates and cell type labels\n",
    "adata = anndata.read_loom(os.path.join(EXPR_PATH, EXPR_MATRIX_FNAME), sparse=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Assign colors to louvain clusters\n",
    "cluster_colors = {\n",
    "    'M1':'#fb8b24',\n",
    "    'M2':'#d90368',\n",
    "    'M3':'#f6bd60',\n",
    "    'ET1':'#56cfe1',\n",
    "    'ET2':'#219ebc',\n",
    "    'ET3':'#846b8a',\n",
    "    'ET4':'#ae25ba',\n",
    "    'PG':'#e637bf',\n",
    "#     'ET5/PG':'#bfadeb',\n",
    "    'T1':'#3700b3'\n",
    "}\n",
    "\n",
    "adata.uns['louvain_colors'] = [cluster_colors[key] for key in sorted(cluster_colors.keys())]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Data with PG nuclei\n",
    "adata"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get all cell ids that are not PG, iow we keep all mitral and tufted cells\n",
    "with open(os.path.join(EXPR_PATH, IDS_NOPG_FNAME)) as infile:\n",
    "    ids_nopg = [line.strip() for line in infile]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Extract only nuclei ids and cluster type, remove PG nuclei\n",
    "seurat_clusters = pd.DataFrame(adata.obs['louvain'].copy()).loc[ids_nopg, :]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Global view on transcriptome Louvain clusters in regulon space"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Re-order within each cluster the nuclei (rows)\n",
    "reordered_clusters = []\n",
    "for cluster_type, nuc_ids in seurat_clusters.groupby('louvain').groups.items():\n",
    "    try:\n",
    "        # Some information to keep in mind\n",
    "        print(\"{:<6}: {:>4}\".format(cluster_type, len(nuc_ids)))\n",
    "        \n",
    "        # Compute clustering of a single cell type\n",
    "        my_auc_dist = sci_distance.pdist(\n",
    "            auc_mtx_Z.loc[nuc_ids.to_list(), :], 'euclidean')\n",
    "        my_auc_linked = sci_hierarchy.linkage(my_auc_dist, 'average')\n",
    "        \n",
    "        # Add the re-ordering of rows to the result\n",
    "        reordered_clusters.extend(\n",
    "            nuc_ids[sci_hierarchy.leaves_list(my_auc_linked)].to_list())\n",
    "        \n",
    "    except KeyError:\n",
    "        print(\"Not present:\", celltype)\n",
    "\n",
    "ordered_auc_mtx_Z = auc_mtx_Z.loc[reordered_clusters, :]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Do not use standard_scale or z_score arguments as the data is already normalized\n",
    "# Put a ridiculous figsize, e.g. (50, 50), if you want to see all regulons and all cells.\n",
    "aux = sns.clustermap(ordered_auc_mtx_Z, \n",
    "                     row_cluster=False, \n",
    "                     col_cluster=False,\n",
    "                     row_colors=seurat_clusters['louvain'].apply(lambda x: cluster_colors[x]),\n",
    "                     cmap=\"coolwarm\", \n",
    "                     vmin=-3, \n",
    "                     vmax=3,\n",
    "                     xticklabels=True,\n",
    "                     yticklabels=False, \n",
    "                     figsize=(14.5, 10));\n",
    "\n",
    "aux.ax_col_dendrogram.remove()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Subcluster identification in regulon space\n",
    "\n",
    "Code is ugly and should be refactored..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Extract UMAP coordinates together with louvain cluster coloring\n",
    "my_umap = pd.DataFrame.from_dict(\n",
    "    {'UMAP_1': adata.obsm['X_umap'][:, 0],\n",
    "     'UMAP_2': adata.obsm['X_umap'][:, 1],\n",
    "     'louvain': adata.obs['louvain']\n",
    "    })\n",
    "\n",
    "my_umap = my_umap.loc[ids_nopg, :]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Here we collect all the mappings from cell id to subclusters\n",
    "my_subclusters = {}\n",
    "\n",
    "tree_cut = 70"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Function for plotting a UMAP\n",
    "def umap_with_subcluster_focus(my_umap, my_subclusters, subcluster_colors):\n",
    "    # Plot all cells first\n",
    "    dotsize = 1.2\n",
    "    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(7, 7))\n",
    "    ax.scatter(my_umap['UMAP_1'], my_umap['UMAP_2'], s=dotsize, color='grey')\n",
    "\n",
    "    # And plot our focal clusters on top\n",
    "    for i, ct_name in enumerate(subcluster_colors.keys()):\n",
    "        ii = my_subclusters[name].loc[my_subclusters[name][cluster_name] == ct_name].index\n",
    "        ax.scatter(my_umap['UMAP_1'][ii], my_umap['UMAP_2'][ii], \n",
    "                   s=dotsize, color=subcluster_colors[ct_name], label=ct_name)\n",
    "\n",
    "    ax.legend(loc='upper right', bbox_to_anchor=(1.5, 1.0), markerscale=2, fontsize='large', frameon=False)\n",
    "    ax.set_xticks([])\n",
    "    ax.set_yticks([]);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Splitting ET1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "name = 'ET1'\n",
    "cluster_name = 'subclusters'\n",
    "\n",
    "# Select cells\n",
    "cell_ids = seurat_clusters[seurat_clusters['louvain'] == name].index.values\n",
    "\n",
    "# Calculate the clustering\n",
    "my_auc_dist = sci_distance.pdist(auc_mtx_Z.loc[cell_ids, :], 'euclidean')\n",
    "my_auc_linked = sci_hierarchy.linkage(my_auc_dist, 'ward')\n",
    "\n",
    "# Cut the tree and visualize on UMAP\n",
    "my_subclusters[name] = pd.DataFrame(sci_hierarchy.fcluster(my_auc_linked, t=tree_cut, criterion='distance'),\n",
    "                    index=cell_ids, columns=[cluster_name])\n",
    "print('Subclusters:', \n",
    "      ', '.join([str(x) for x in sorted(my_subclusters[name][cluster_name].unique().tolist())]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# First attempt at labelling the clusters\n",
    "to_name = {\n",
    "    1: 'ET1a',\n",
    "    2: 'ET1b'\n",
    "}\n",
    "\n",
    "my_subclusters[name][cluster_name].replace(to_name, inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subcluster_colors = {\n",
    "    'ET1a': 'red',\n",
    "    'ET1b': 'blue'\n",
    "}\n",
    "umap_with_subcluster_focus(my_umap, my_subclusters, subcluster_colors)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Single cell type heatmap to see different subclusters in regulon space\n",
    "aux = sns.clustermap(auc_mtx_Z.loc[cell_ids, :], \n",
    "                     method='ward', \n",
    "                     metric='euclidean', \n",
    "                     row_linkage=my_auc_linked, \n",
    "                     col_cluster=True,\n",
    "                     row_colors=my_subclusters[name][cluster_name].apply(lambda x: subcluster_colors[x]),\n",
    "                     cmap=\"coolwarm\", \n",
    "                     vmin=-4.0, vmax=4.0, \n",
    "                     yticklabels=False, figsize=(12.5, 5));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Splitting ET2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "name = 'ET2'\n",
    "\n",
    "cell_ids = seurat_clusters[seurat_clusters['louvain'] == name].index.values\n",
    "\n",
    "# Calculate the clustering\n",
    "my_auc_dist = sci_distance.pdist(auc_mtx_Z.loc[cell_ids, :], 'euclidean')\n",
    "my_auc_linked = sci_hierarchy.linkage(my_auc_dist, 'ward')\n",
    "\n",
    "# Cut the tree and visualize on UMAP\n",
    "my_subclusters[name] = pd.DataFrame(sci_hierarchy.fcluster(my_auc_linked, t=tree_cut, criterion='distance'),\n",
    "                    index=cell_ids, columns=[cluster_name])\n",
    "\n",
    "print('Subclusters:', \n",
    "      ', '.join([str(x) for x in sorted(my_subclusters[name][cluster_name].unique().tolist())]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# First attempt at labelling the coarse clusters\n",
    "to_name = {\n",
    "    1: 'ET2a',\n",
    "    2: 'ET2b',\n",
    "    3: 'ET2c'\n",
    "}\n",
    "\n",
    "my_subclusters[name][cluster_name].replace(to_name, inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subcluster_colors = {\n",
    "    'ET2a': 'red',\n",
    "    'ET2b': 'blue',\n",
    "    'ET2c': 'green'\n",
    "}\n",
    "\n",
    "umap_with_subcluster_focus(my_umap, my_subclusters, subcluster_colors)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "aux = sns.clustermap(auc_mtx_Z.loc[cell_ids, :], method='ward', metric='euclidean', \n",
    "            row_linkage=my_auc_linked, col_cluster=True,\n",
    "            row_colors=my_subclusters[name][cluster_name].apply(lambda x: subcluster_colors[x]),\n",
    "            cmap=\"coolwarm\", vmin=-4.0, vmax=4.0, yticklabels=False, figsize=(12.5, 5));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Splitting ET4"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "name = 'ET4'\n",
    "\n",
    "cell_ids = seurat_clusters[seurat_clusters['louvain'] == name].index.values\n",
    "\n",
    "# Calculate the clustering\n",
    "my_auc_dist = sci_distance.pdist(auc_mtx_Z.loc[cell_ids, :], 'euclidean')\n",
    "my_auc_linked = sci_hierarchy.linkage(my_auc_dist, 'ward')\n",
    "\n",
    "# Cut the tree and visualize on UMAP\n",
    "my_subclusters[name] = pd.DataFrame(sci_hierarchy.fcluster(my_auc_linked, t=tree_cut, criterion='distance'),\n",
    "                    index=cell_ids, columns=[cluster_name])\n",
    "\n",
    "print('Subclusters:', \n",
    "      ', '.join([str(x) for x in sorted(my_subclusters[name][cluster_name].unique().tolist())]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# First attempt at labelling the coarse clusters\n",
    "to_name = {\n",
    "    1: 'ET4a',\n",
    "    2: 'ET4b'\n",
    "}\n",
    "\n",
    "my_subclusters[name][cluster_name].replace(to_name, inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subcluster_colors = {\n",
    "    'ET4a': 'red',\n",
    "    'ET4b': 'blue'\n",
    "}\n",
    "\n",
    "umap_with_subcluster_focus(my_umap, my_subclusters, subcluster_colors)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "aux = sns.clustermap(auc_mtx_Z.loc[cell_ids, :], method='ward', metric='euclidean', \n",
    "            row_linkage=my_auc_linked, col_cluster=True,\n",
    "            row_colors=my_subclusters[name][cluster_name].apply(lambda x: subcluster_colors[x]),\n",
    "            cmap=\"coolwarm\", vmin=-4.0, vmax=4.0, yticklabels=False, figsize=(12.5, 5));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Splitting M1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "name = 'M1'\n",
    "\n",
    "# Select the cells\n",
    "cell_ids = seurat_clusters[seurat_clusters['louvain'] == name].index.values\n",
    "\n",
    "# Calculate the clustering\n",
    "my_auc_dist = sci_distance.pdist(auc_mtx_Z.loc[cell_ids, :], 'euclidean')\n",
    "my_auc_linked = sci_hierarchy.linkage(my_auc_dist, 'ward')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Cut the tree and visualize on UMAP\n",
    "my_subclusters[name] = pd.DataFrame(sci_hierarchy.fcluster(my_auc_linked, t=tree_cut, criterion='distance'),\n",
    "                    index=cell_ids, columns=[cluster_name])\n",
    "\n",
    "print('Subclusters:', \n",
    "      ', '.join([str(x) for x in sorted(my_subclusters[name][cluster_name].unique().tolist())]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# First attempt at labelling the coarse clusters\n",
    "to_name = {\n",
    "    1: 'M1a',\n",
    "    2: 'M1b',\n",
    "    3: 'M1c',\n",
    "    4: 'M1d'\n",
    "}\n",
    "\n",
    "my_subclusters[name][cluster_name].replace(to_name, inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subcluster_colors = {\n",
    "    'M1a': 'red',\n",
    "    'M1b': 'blue',\n",
    "    'M1c': 'green',\n",
    "    'M1d': 'cyan'\n",
    "}\n",
    "\n",
    "umap_with_subcluster_focus(my_umap, my_subclusters, subcluster_colors)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "aux = sns.clustermap(auc_mtx_Z.loc[cell_ids, :], method='ward', metric='euclidean', \n",
    "            row_linkage=my_auc_linked, col_cluster=True,\n",
    "            row_colors=my_subclusters[name][cluster_name].apply(lambda x: subcluster_colors[x]),\n",
    "            cmap=\"coolwarm\", vmin=-4.0, vmax=4.0, yticklabels=False, figsize=(12.5, 5));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Splitting M2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "name = 'M2'\n",
    "cell_ids = seurat_clusters[seurat_clusters['louvain'] == name].index.values\n",
    "\n",
    "# Calculate the clustering\n",
    "my_auc_dist = sci_distance.pdist(auc_mtx_Z.loc[cell_ids, :], 'euclidean')\n",
    "my_auc_linked = sci_hierarchy.linkage(my_auc_dist, 'ward')\n",
    "\n",
    "# Cut the tree and visualize on UMAP\n",
    "my_subclusters[name] = pd.DataFrame(sci_hierarchy.fcluster(my_auc_linked, t=tree_cut, criterion='distance'),\n",
    "                    index=cell_ids, columns=[cluster_name])\n",
    "\n",
    "print('Subclusters:', \n",
    "      ', '.join([str(x) for x in sorted(my_subclusters[name][cluster_name].unique().tolist())]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# First attempt at labelling the coarse clusters\n",
    "to_name = {\n",
    "    1: 'M2a',\n",
    "    2: 'M2b',\n",
    "    3: 'M2c'\n",
    "}\n",
    "\n",
    "my_subclusters[name][cluster_name].replace(to_name, inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subcluster_colors = {\n",
    "    'M2a': 'cyan',\n",
    "    'M2b': 'red',\n",
    "    'M2c': 'green'\n",
    "}\n",
    "\n",
    "umap_with_subcluster_focus(my_umap, my_subclusters, subcluster_colors)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "aux = sns.clustermap(auc_mtx_Z.loc[cell_ids, :], method='ward', metric='euclidean', \n",
    "            row_linkage=my_auc_linked, col_cluster=True,\n",
    "            row_colors=my_subclusters[name][cluster_name].apply(lambda x: subcluster_colors[x]),\n",
    "            cmap=\"coolwarm\", vmin=-4.0, vmax=4.0, yticklabels=False, figsize=(12.5, 5));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Splitting M3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "name = 'M3'\n",
    "\n",
    "cell_ids = seurat_clusters[seurat_clusters['louvain'] == name].index.values\n",
    "\n",
    "# Calculate the clustering\n",
    "my_auc_dist = sci_distance.pdist(auc_mtx_Z.loc[cell_ids, :], 'euclidean')\n",
    "my_auc_linked = sci_hierarchy.linkage(my_auc_dist, 'ward')\n",
    "\n",
    "# Cut the tree and visualize on UMAP\n",
    "my_subclusters[name] = pd.DataFrame(sci_hierarchy.fcluster(my_auc_linked, t=tree_cut, criterion='distance'),\n",
    "                    index=cell_ids, columns=[cluster_name])\n",
    "\n",
    "print('Subclusters:', \n",
    "      ', '.join([str(x) for x in sorted(my_subclusters[name][cluster_name].unique().tolist())]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# First attempt at labelling the coarse clusters\n",
    "to_name = {\n",
    "    1: 'M3'\n",
    "}\n",
    "\n",
    "my_subclusters[name][cluster_name].replace(to_name, inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subcluster_colors = {\n",
    "    'M3': 'green'\n",
    "}\n",
    "\n",
    "umap_with_subcluster_focus(my_umap, my_subclusters, subcluster_colors)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "aux = sns.clustermap(auc_mtx_Z.loc[cell_ids, :], method='ward', metric='euclidean', \n",
    "            row_linkage=my_auc_linked, col_cluster=True,\n",
    "            row_colors=my_subclusters[name][cluster_name].apply(lambda x: subcluster_colors[x]),\n",
    "            cmap=\"coolwarm\", vmin=-4.0, vmax=4.0, yticklabels=False, figsize=(12.5, 5));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Splitting T1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "name = 'T1'\n",
    "\n",
    "# Select cells\n",
    "cell_ids = seurat_clusters[seurat_clusters['louvain'] == name].index.values\n",
    "\n",
    "# Calculate the clustering\n",
    "my_auc_dist = sci_distance.pdist(auc_mtx_Z.loc[cell_ids, :], 'euclidean')\n",
    "my_auc_linked = sci_hierarchy.linkage(my_auc_dist, 'ward')\n",
    "\n",
    "# Cut the tree and visualize on UMAP\n",
    "my_subclusters[name] = pd.DataFrame(sci_hierarchy.fcluster(my_auc_linked, t=tree_cut, criterion='distance'),\n",
    "                    index=cell_ids, columns=[cluster_name])\n",
    "\n",
    "print('Subclusters:', \n",
    "      ', '.join([str(x) for x in sorted(my_subclusters[name][cluster_name].unique().tolist())]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# First attempt at labelling the coarse clusters\n",
    "to_name = {\n",
    "    1: 'T1a',\n",
    "    2: 'T1b',\n",
    "    3: 'T1c'\n",
    "}\n",
    "\n",
    "my_subclusters[name][cluster_name].replace(to_name, inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "subcluster_colors = {\n",
    "    'T1a': 'blue',\n",
    "    'T1b': 'cyan',\n",
    "    'T1c': 'green'\n",
    "}\n",
    "\n",
    "umap_with_subcluster_focus(my_umap, my_subclusters, subcluster_colors)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Do not use standard_scale or z_score arguments as the data is already normalized\n",
    "# Put a ridiculous figsize, e.g. (50, 50), if you want to see all regulons and all cells.\n",
    "aux = sns.clustermap(auc_mtx_Z.loc[cell_ids, :], method='ward', metric='euclidean', \n",
    "            row_linkage=my_auc_linked, col_cluster=True,\n",
    "            row_colors=my_subclusters[name][cluster_name].apply(lambda x: subcluster_colors[x]),\n",
    "            cmap=\"coolwarm\", vmin=-4.0, vmax=4.0, yticklabels=False, figsize=(12.5, 5));\n",
    "\n",
    "# aux.savefig(os.path.join(\"figures\", \"regulon_activity_matrix_{}.pdf\".format(cutoff)), pad_inches=0.01)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Putting the clusters back together"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "seurat_subclusters = pd.concat(my_subclusters.values())\n",
    "seurat_clusters.update(seurat_subclusters.rename(columns={'subclusters': 'louvain'}))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Double check all subclusters are present\n",
    "sorted(np.unique(seurat_clusters['louvain'].values))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We do not save PG\n",
    "seurat_clusters = seurat_clusters[seurat_clusters['louvain'] != 'PG']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(os.path.join(REGULONS_PATH, 'refining_seurat_clustering_with_regulons.csv'), 'w') as outfile:\n",
    "    seurat_clusters.to_csv(outfile, sep='\\t')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
